<?php

namespace Adduc\Howl\Entity;

use DateTime;
use stdClass;

class Photo extends Entity
{
    /** @property int */
    public $id;
    
    /** @property string */
    public $name;

    public $description;
    
    /** @property string */
    public $url;

    /** @property int */
    public $width;

    /** @property int */
    public $height;

    public $legacy_id;

    /** @property DateTime */
    public $created_at;

    /** @property DateTime */
    public $updated_at;

    public $episode_id;
    
    /** @property string */
    public $small_url;

    /** @property string */
    public $medium_url;

    public $large_url;
    
    /** @property string */
    public $thumbnail_url;

    public $url_file_name;
    public $url_content_type;
    public $url_file_size;
    
    /** @property DateTime */
    public $url_updated_at;

    public $small_url_file_name;
    public $small_url_content_type;
    public $small_url_file_size;
    
    /** @property DateTime */
    public $small_url_updated_at;

    public $medium_url_file_name;
    public $medium_url_content_type;
    public $medium_url_file_size;

    /** @property DateTime */
    public $medium_url_updated_at;

    public $large_url_file_name;
    public $large_url_content_type;
    public $large_url_file_size;

    /** @property DateTime */
    public $large_url_updated_at;

    public $thumbnail_url_file_name;
    public $thumbnail_url_content_type;
    public $thumbnail_url_file_size;

    /** @property DateTime */
    public $thumbnail_url_updated_at;

    /** @property int */
    public $show_id;

    public function __construct(array $data)
    {
        parent::__construct($data);
        $this->created_at = new DateTime($this->created_at);
        $this->updated_at = new DateTime($this->updated_at);
        $this->url_updated_at = new DateTime($this->url_updated_at);
        $this->small_url_updated_at = new DateTime($this->small_url_updated_at);
        $this->medium_url_updated_at = new DateTime($this->medium_url_updated_at);
        $this->large_url_updated_at = new DateTime($this->large_url_updated_at);
        $this->thumbnail_url_updated_at = new DateTime($this->thumbnail_url_updated_at);
    }
}
