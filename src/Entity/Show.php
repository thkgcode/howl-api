<?php

namespace Adduc\Howl\Entity;

use DateTime;
use stdClass;

class Show extends Entity
{
    /** @property int */
    public $id;

    /** @property string */
    public $name;

    /** @property string */
    public $description;

    /** @property int */
    public $explicit;

    public $legacy_id;

    /** @property int */
    public $network_id;

    /** @property DateTime */
    public $created_at;

    /** @property DateTime */
    public $updated_at;

    /** @property int */
    public $stagebloc_account_id;

    public $key_image_url_large;
    public $key_image_url_medium;
    public $key_image_url_small;
    public $key_image_url_thumb;
    public $featured;
    public $miniseries;
    public $album;
    public $specials;
    public $special;
    public $featured_display_order;

    /** @property string */
    public $sort_direction;

    public $single;
    public $display_in_menu;
    public $wide_artwork_url_large;
    public $wide_artwork_url_medium;
    public $wide_artwork_url_small;
    public $wide_artwork_url_thumb;

    /** @property DateTime */
    public $published_at;

    /** @property string */
    public $artwork_url;

    /** @property string */
    public $url_slug;

    /** @property string */
    public $url;

    /** @property int */
    public $episodes_count;

    /** @property string */
    public $artwork_url_thumb;

    /** @property string */
    public $artwork_url_small;

    /** @property string */
    public $artwork_url_medium;

    /** @property string */
    public $artwork_url_large;

    /** @property int */
    public $twitter_list_id;

    /** @property string */
    public $hashtag;

    /** @property int */
    public $tier_required;

    public $sales_page_background_image;
    public $audio_playlist_id;
    public $show_page_synopsis;
    public $free_episode_ids;
    public $audio_preview;

    /** @property Photo[] */
    public $host_photos;

    public $just_added;
    public $just_added_display_order;
    public $seasons;
    public $earwolf;
    public $featured_collection;
    public $featured_collection_display_order;

    /** @property Episode[] */
    public $episodes = [];

    public function __construct(array $data)
    {
        if (isset($data['metadata']) && isset($data['data'])) {
            $data = $data['metadata'] + $data['data'];
        }

        parent::__construct($data);
        $this->created_at = new DateTime($this->created_at);
        $this->updated_at = new DateTime($this->updated_at);
        $this->published_at = new DateTime($this->published_at);
        foreach ($this->host_photos ?: [] as $key => $host_photo) {
            $this->host_photos[$key] = new Photo($host_photo);
        }
        foreach ($this->episodes ?: [] as $key => $episode) {
            $this->episodes[$key] = new Episode($episode);
        }
    }

    /**
     * Returns the link for the show.
     *
     * @return string
     */
    public function getLink()
    {
        if ($this->url) {
            return $this->url;
        } elseif ($this->url_slug) {
            return 'http://www.earwolf.com/show/' . $this->url_slug;
        } else {
            return 'http://www.earwolf.com';
        }
    }

    /**
     * Identifies the highest-quality image available.
     *
     * @return string|false
     */
    public function getImageUrl()
    {
        switch (true) {
            case $this->artwork_url:
                return $this->artwork_url;
            case $this->artwork_url_large:
                return $this->artwork_url_large;
            case $this->artwork_url_medium:
                return $this->artwork_url_medium;
            case $this->artwork_url_small:
                return $this->artwork_url_small;
            case $this->artwork_url_thumb:
                return $this->artwork_url_thumb;
            default:
                return false;
        }
    }

    /**
     * Identifies the lowest-quality image available.
     *
     * @return string|false
     */
    public function getThumbUrl()
    {
        switch (true) {
            case $this->artwork_url_thumb:
                return $this->artwork_url_thumb;
            case $this->artwork_url_small:
                return $this->artwork_url_small;
            case $this->artwork_url_medium:
                return $this->artwork_url_medium;
            case $this->artwork_url_large:
                return $this->artwork_url_large;
            case $this->artwork_url:
                return $this->artwork_url;
            default:
                return false;
        }
    }
}
