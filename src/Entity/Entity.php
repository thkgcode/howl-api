<?php

namespace Adduc\Howl\Entity;

use stdClass;

abstract class Entity
{
    /**
     * @param stdClass $data
     */
    public function __construct(array $data = [])
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            } else {
                trigger_error(get_called_class() . "::\${$key} doesn't exist.");
            }
        }
    }
}
