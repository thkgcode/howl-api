<?php

namespace Adduc\Howl\Entity;

use DateTime;
use stdClass;

// Earwolf, Wolfpop, Howl Premium, etc.
class Network extends Entity
{
    /** @property int */
    public $id;

    /** @property string */
    public $name;

    /** @property DateTime */
    public $created_at;

    /** @property DateTime */
    public $updated_at;

    /** @property int */
    public $shows_count;

    /** @property Show[] */
    public $shows;

    public $featured;
    public $display_in_menu;
    public $description;
    public $tier_required;
    public $treat_as_show;
    public $square_artwork_url_large;
    public $square_artwork_url_medium;
    public $square_artwork_url_small;
    public $square_artwork_url_thumb;
    public $key_image_url_large;
    public $key_image_url_medium;
    public $key_image_url_small;
    public $key_image_url_thumb;
    public $wide_artwork_url_large;
    public $wide_artwork_url_medium;
    public $wide_artwork_url_small;
    public $wide_artwork_url_thumb;

    public function __construct(array $data)
    {
        parent::__construct($data);

        $this->created_at = new DateTime($this->created_at);
        $this->updated_at = new DateTime($this->updated_at);

        foreach ($this->shows as $key => $show) {
            $this->shows[$key] = new Show($show['show']['data']);
        }
    }
}
